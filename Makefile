SBIN_DIR = $(DESTDIR)/usr/sbin
UNIT_DIR = $(DESTDIR)/usr/lib/systemd/system
MAN8_DIR = $(DESTDIR)/usr/share/man/man8
DOCS_DIR = $(DESTDIR)/usr/share/doc/fake-hwclock
CRON_DIR = $(DESTDIR)/etc/cron.hourly
DEFL_DIR = $(DESTDIR)/etc/default

install:
	$(INSTALL) -d			\
		$(SBIN_DIR)		\
		$(UNIT_DIR)		\
		$(MAN8_DIR)		\
		$(DOCS_DIR)		\
		$(CRON_DIR)		\
		$(DEFL_DIR)		\
		#
	$(INSTALL) -m 0755 fake-hwclock $(SBIN_DIR)
	$(INSTALL) -m 0644 fake-hwclock.8 $(MAN8_DIR)
	$(INSTALL) -m 0644 etc/default/fake-hwclock $(DEFL_DIR)
	$(INSTALL) -m 0644 debian/changelog $(DOCS_DIR)
	$(INSTALL) -m 0644 debian/copyright $(DOCS_DIR)
	$(INSTALL) -m 0644 debian/fake-hwclock.service $(UNIT_DIR)
	$(INSTALL) -m 0755 debian/fake-hwclock.cron.hourly $(CRON_DIR)/fake-hwclock

