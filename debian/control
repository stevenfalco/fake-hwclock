Source: fake-hwclock
Section: admin
Priority: optional
Maintainer: Steve McIntyre <93sam@debian.org>
Build-Depends: debhelper (>= 10)
Standards-Version: 4.5.0
Vcs-Browser: https://git.einval.com/cgi-bin/gitweb.cgi?p=fake-hwclock.git
Vcs-Git: https://git.einval.com/git/fake-hwclock.git

Package: fake-hwclock
Architecture: all
Depends: ${misc:Depends}
Suggests: cron | cron-daemon, ntp
Description: Save/restore system clock on machines without working RTC hardware
 Some machines don't have a working realtime clock (RTC) unit, or no
 driver for the hardware that does exist. fake-hwclock is a simple set
 of scripts to save the kernel's current clock periodically (including
 at shutdown) and restore it at boot so that the system clock keeps at
 least close to realtime. This will stop some of the problems that may
 be caused by a system believing it has travelled in time back to
 1970, such as needing to perform filesystem checks at every boot.
 .
 On top of this, use of NTP is still recommended to deal with the fake
 clock "drifting" while the hardware is halted or rebooting.
