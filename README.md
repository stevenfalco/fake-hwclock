fake-hwclock - A fake hardware clock to set the initial date and time
=====================================================================

As purchased, Raspberry Pi computers do not include a hardware real-time clock chip.  Thus, each time they boot, they don't have the correct date and time of day until an NTP client gets the time from a networked server.  That causes incorrect timestamps in system log files during boot.

As a work-around, **_fake-hwclock_** is configured to periodically save the current date/time to a file.  When the system reboots, the saved date/time is used as a temporary value, until an NTP client can set the exact time.

The official Raspberry Pi OS already contains the fake-hwclock package, but not every OS does.  For Debian Linux derived systems, the official fake-hwclock can usually be installed without difficulty.

However, Fedora Linux is not derived from Debian Linux and doesn't have the fake-hwclock package.  Thus, this version of fake-hwclock provides a package for Fedora Linux.  It derives from https://git.einval.com/git/fake-hwclock.git and differs primarily in adding an RPM spec file to make it easier to use on RPM-based systems like Fedora Linux.
