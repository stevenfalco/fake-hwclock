Name:           fake-hwclock
Version:        0.12_falco
Release:        1%{?dist}
Summary:        Fake HW Clock

License:        GPLv2

Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  make
BuildRequires:  systemd

Requires:       crontabs
Requires:       systemd


%description
Set and save date/time on systems without an RTC.


%prep
%setup -q


%install
%{make_install}


%files
%{_sbindir}/%{name}
%{_unitdir}/%{name}.service
%{_mandir}/man8/*
%{_docdir}/%{name}

%config(noreplace) %{_sysconfdir}/default/%{name}
%config(noreplace) %{_sysconfdir}/cron.hourly/%{name}

%license COPYING


%changelog
* Thu Aug 04 2022 Steven A. Falco <stevenfalco@gmail.com> - 0.12_falco-1
- Initial release

